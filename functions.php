<?php

if ( ! function_exists( 'daily_feed_setup' ) ) :
/**
 * Sets up theme defaults and registers support for various WordPress features.
 *
 * Note that this function is hooked into the after_setup_theme hook, which
 * runs before the init hook. The init hook is too late for some features, such
 * as indicating support for post thumbnails.
 */
function daily_feed_setup() {
	/*
	 * Enable support for Post Thumbnails on posts and pages.
	 *
	 * @link https://developer.wordpress.org/themes/functionality/featured-images-post-thumbnails/
	 */
	add_theme_support( 'post-thumbnails' );

	// Add custom image sizes
	add_image_size( 'article', 700, 791, array( 'center', 'top' ) );
	add_image_size( 'carousel', 840, 473, array( 'center', 'top' ) );
	add_image_size( 'large', 1600, 1200, array( 'center', 'top' ) );
	add_image_size( 'listing-thumb', 411, 226, array( 'center', 'top' ) );
	add_image_size( 'sidebar-thumb', 72, 72, array( 'center', 'top' ) );


	/*********************************/
	/* Change Search Button Text
	/**************************************/



	add_filter( 'get_the_archive_title', function ($title) {

    	if ( is_category() ) {
            $title = single_cat_title( '', false );
        }
        elseif ( is_tag() ) {
            $title = single_tag_title( '', false );
        }

	    return $title;
	});




	// Add default posts and comments RSS feed links to head.
	add_theme_support( 'automatic-feed-links' );

	/*
	 * Let WordPress manage the document title.
	 * By adding theme support, we declare that this theme does not use a
	 * hard-coded <title> tag in the document head, and expect WordPress to
	 * provide it for us.
	 */
	add_theme_support( 'title-tag' );

	// This theme uses wp_nav_menu() in one location.
	register_nav_menus( array(
		'primary' => esc_html__( 'Primary', 'daily-feed' ),
	) );

	/*
	 * Switch default core markup for search form, comment form, and comments
	 * to output valid HTML5.
	 */
	add_theme_support( 'html5', array(
		'search-form',
		'comment-form',
		'comment-list',
		'gallery',
		'caption',
	) );

	// Set up the WordPress core custom background feature.
	add_theme_support( 'custom-background', apply_filters( 'daily_feed_custom_background_args', array(
		'default-color' => 'ffffff',
		'default-image' => '',
	) ) );
}
endif;
add_action( 'after_setup_theme', 'daily_feed_setup' );

/**
 * Enqueue scripts and styles.
 */
function daily_feed_scripts() {

	wp_enqueue_style( 'daily-feed-style', get_stylesheet_uri() );


	wp_enqueue_script( 'daily-feed-modules', get_template_directory_uri() . '/assets/js/modules.min.js', array(), '20151215', true );
	wp_enqueue_script( 'daily-feed-lib', get_template_directory_uri() . '/assets/js/libraries.min.js', array(), '20151215', true );
	wp_enqueue_script( 'daily-feed-scripts', get_template_directory_uri() . '/assets/js/scripts.min.js', array(), '20151215', true );
	if ( ! has_tag( 'infinitescroll' ) ) {
		wp_enqueue_script( 'daily-feed-infinite-scroll', get_template_directory_uri() . '/assets/js/infinite-scroll.js', array(), '20151215', true );
	}
	/*
	if ( is_singular() && comments_open() && get_option( 'thread_comments' ) ) {
		wp_enqueue_script( 'comment-reply' );
	}
	*/


}
add_action( 'wp_enqueue_scripts', 'daily_feed_scripts', 1 );

add_filter( 'get_the_archive_title', function ($title) {

		if ( is_category() ) {
					$title = single_cat_title( '', false );
			}
			elseif ( is_tag() ) {
					$title = single_tag_title( '', false );
			}

		return $title;
});

// Get Main Post Image
function getPostImage($post, $size = 'full'){

	if(is_object($post)){
		$post = $post->id;
	}

	$image = get_template_directory_uri() . '/assets/images/placeholder.png';

	if(has_post_thumbnail($post)){

		$image = wp_get_attachment_image_src(get_post_thumbnail_id($post), $size, true);

	}

	return is_array($image) ? $image[0] : $image;

}

// Get the first category for display
function getPostDisplayCategory($post){

	if(is_object($post)){
		$post = $post->id;
	}

	$categories = wp_get_post_categories($post, array(
		'fields'=>'names'
	));

	return isset($categories[0]) ? $categories[0] : '';

}

// Display pagination for individual posts
function daily_feed_post_pages($nextPost = ''){

	global $numpages;
	global $page;
	global $post;

	if($numpages == 1 && empty($nextPost)){
		return;
	}

	$back = false;
	$next = false;

	if($page > 1){

		$back = true;

	}

	if($page < $numpages && $page != $numpages){

		$next = true;

	}

	$parts = explode("?", $_SERVER[REQUEST_URI]);
	$qutm .= '?';
	if (isset($parts[1])) {
        $qutmresults = $qutm."".$parts[1];
	$qutmresults = str_replace(array('&lazy=true', '?lazy=true'), '', $qutmresults);
		}

	$content = '<div class="post-pagination">';
	if($back){

		if($page === 2){
			//$content .= '<a class="post-pagination__prev" href="' . get_permalink($post) . '"><</a>';
		}else{
			//$content .= '<a class="post-pagination__prev" href="' . get_permalink($post) .'' . ($page - 1) . '/' . $qutmresults .'"><</a>';
		}

	}
	//$content .= '<p class="post-pagination__count">Page ' . $page . '/' . $numpages . '</p>';

	if($next){

		$content .= '<a class="post-pagination__next" style="text-align:center;width:100%;" href="' . get_permalink($post) .'' . ($page + 1) . '/' . $qutmresults .'">Next Page</a>';

	if(!empty($nextPost)){
			
			$content .= '<a class="pagination__nextpost" style="display:none;" href="' . esc_url( get_permalink( $nextPost->ID ) ) .'/' . $qutmresults .'"></a>';

		}

	}else{

		if(!empty($nextPost)){

			$content .= '<a class="post-pagination__next pagination__nextpost" style="text-align:center;width:100%;" href="' . esc_url( get_permalink( $nextPost->ID ) ) .'/' . $qutmresults .'">Next Post</a>';

		}

	}

	//$content .= '<a class="post-pagination__next" style="text-align:center;width:100%;" href="' . get_permalink($post) .'' . ($page + 1) . '/' . $qutmresults .'">Next Page</a>';
	$content .= '</div><br/><p class="paginationad">Advertisement</p><div id="RTK_wiHk" style="overflow:hidden"></div>';

	return $content;

}

/*
function wpdocs_dequeue_script() {
   wp_deregister_script( 'jquery-core' );

}
add_action( 'wp_print_scripts', 'wpdocs_dequeue_script', 100 );
*/

add_filter( 'single_template', function( $template ) {

    global $post;

		$mediumTag = (isset($_GET['utm_medium']) ? $_GET['utm_medium'] : '');



    if ( $mediumTag == 'desktop' ) {

        $locate_template = locate_template( "single-utm-page.php" );
        if ( ! empty( $locate_template ) ) {
            $template = $locate_template;
        }

    }

    return $template;
});

function df_insert_ad_containers( $content ) {
    if ( is_home() || is_front_page() )
        return $content;

    $needle = '<div id="RTK_';
    $offset = strpos( $content, 'class="container"' );
    while ( ( $pos = strpos( $content, $needle, $offset ) ) !== FALSE ) {
	$offset = $pos + 45;
	$uid = uniqid();
	$content = substr_replace( $content, '<div id="' . $uid . '"></div>', $pos, 0 );
    }
    return $content;
}
