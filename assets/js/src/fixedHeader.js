function fixHeader() {

  $(document).on('scroll', function() {
    if($(this).scrollTop() >= 105) {
      $('.site-header, .site-content').addClass('fixed-nav');
    } else {
      $('.site-header, .site-content').removeClass('fixed-nav');
    }
  });

}

fixHeader();
