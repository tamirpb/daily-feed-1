<?php
  if(!$_GET['lazy']){
 ?>
<footer class="site-footer">
  <a class="site-footer__logo" href="">
    <?php include "assets/svgs/logo.svg" ?>
  </a>

  <nav class="site-footer__nav">
    <ul class="footer-nav">
      <li><a class="site-footer__nav-items" href="/cookie-policy">Cookie Policy</a></li>
      <li><a class="site-footer__nav-items" href="/contact">Contact</a></li>
      <li><a class="site-footer__nav-items" href="/about">About</a></li>
      <li><a class="site-footer__nav-items" href="/sitemap">Sitemap</a></li>
      <li><a class="site-footer__nav-items" href="/terms">Terms</a></li>
      <li><a class="site-footer__nav-items" href="/privacy-policy">Privacy</a></li>
    </ul>
  </nav>
</footer>

<div id="newsletter-subscribe">
<?php

if($_SERVER['HTTP_HOST'] == 'www.dailyfeed.co.uk'){

  echo do_shortcode('[contact-form-7 id="114190"]');

}elseif($_SERVER['HTTP_HOST'] == 'dailyfeed.supremodevelopment.co.uk'){

  echo do_shortcode('[contact-form-7 id="111582"]');

}elseif($_SERVER['HTTP_HOST'] == 'dailyfeed.dev'){

  echo do_shortcode('[contact-form-7 id="111584"]');

}

?>
</div>

<?php wp_footer(); ?>

<script>
    if(typeof System !== 'undefined'){
      System['import'].call(System, 'app');
    }
</script>

<script>
jQuery(document).ready(function()
{
if( jQuery(window).width() > 960 )
{
  jQuery('[id*="rcjsload"]').remove();
  jQuery('.advertisement').remove();
  jQuery('.code-block').remove();
  jQuery('[id*="in-article"]').remove();
}
});
</script>

</body>
</html>
<?php
}
?>